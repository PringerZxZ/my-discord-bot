class UnitConversion:
    def __init__(self, unit_num):
        self.unit_num = unit_num

    # Temperature Conversion Section
    def ftoc(self):
        try:
            return (str(round((int(self.unit_num) - 32) * 5/9)) + "° C")
        except ValueError:
            return "Please enter a valid numerical value."

    def ctof(self):
        try:
            return (str(round((int(self.unit_num) * 9/5) + 32)) + "° F")
        except ValueError:
            return "Please enter a valid numerical value."

    def ftok(self):
        try:
            return (str(round((int(self.unit_num) - 32) * 5/9 + 273.15)) + " K")
        except ValueError:
            return "Please enter a valid numerical value."

    def ctok(self):
        try:
            return (str(round(int(self.unit_num) + 273.15)) + " K")
        except ValueError:
            return "Please enter a valid numerical value."
            
    def ktof(self):
        try:
            return (str(round((int(self.unit_num) - 273.15) * 9/5 + 32)) + "° C")
        except ValueError:
            return "Please enter a valid numerical value."
            
    def ktoc(self):
        try:
            return (str(round(int(self.unit_num) - 273.15)) + "° F")
        except ValueError:
            return "Please enter a valid numerical value."
            
    # Metric-Imperial Conversion Section

    # Metric to Imperial
    def kmtomi(self):
        try:
            return (str(round(int(self.unit_num) * 0.62, 3)) + " mi.")
        except ValueError:
            return "Please enter a valid numerical value."
            
    def kmtoft(self):
        try:
            return (str(round(int(self.unit_num) * 3280.8, 3)) + " ft.")
        except ValueError:
            return "Please enter a valid numerical value."
            
    def mtoft(self):
        try:
            return (str(round(int(self.unit_num) * 3.28, 3)) + " ft.")
        except ValueError:
            return "Please enter a valid numerical value."
            
    def cmtoin(self):
        try:
            return (str(round(int(self.unit_num) * 0.39, 3)) + " in.")
        except ValueError:
            return "Please enter a valid numerical value."
            
    def mmtoin(self):
        try:
            return (str(round(int(self.unit_num) * 0.039, 3)) + " in.")
        except ValueError:
            return "Please enter a valid numerical value."
               
    def ltoqt(self):
        try:
            return (str((round(int(self.unit_num) * 1.057, 3))) + " qt.")
        except ValueError:
            return "Please enter a valid numerical value."
            
    def ltogal(self):
        try:
            return (str(round(int(self.unit_num) * 0.264, 3)) + " gal.")
        except ValueError:
            return "Please enter a valid numerical value."
            
    def mltoc(self):
        try:
            return (str(round(int(self.unit_num) * 0.0042, 3)) + " c.")
        except ValueError:
            return "Please enter a valid numerical value."
            
    def mltooz(self):
        try:
            return (str(round(int(self.unit_num) * 0.0338, 3)) + " fl. oz.")
        except ValueError:
            return "Please enter a valid numerical value."
            
    def kgtot(self):
        try:
            return (str(round(int(self.unit_num) * 0.0011, 3)) + " T.")
        except ValueError:
            return "Please enter a valid numerical value."
            
    def kgtolb(self):
        try:
            return (str(round(int(self.unit_num) * 2.2046, 3)) + " lb.")
        except ValueError:
            return "Please enter a valid numerical value."
            
    def gtooz(self):
        try:
            return (str(round(int(self.unit_num) * 0.035, 3)) + " oz.")
        except ValueError:
            return "Please enter a valid numerical value."
            
    def gtolb(self):
        try:
            return (str(round(int(self.unit_num) * 0.002205, 3)) + " lb.")
        except ValueError:
            return "Please enter a valid numerical value."
            
    def mgtooz(self):
        try:
            return (str(round(int(self.unit_num) * 0.000035, 3)) + " oz.")
        except ValueError:
            return "Please enter a valid numerical value."
            
    # Imperial to Metric