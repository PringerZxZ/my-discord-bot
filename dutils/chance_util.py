import random

class Dice:
    dice_result = []
    dice_dict = {"/d3": 3, "/d4": 4, "/d6": 6, "/d8": 8, "/d10": 10, "/d12": 12, "/d20": 20, "/d100": 100}
    

    def dice(self, message):
        self.message = message
        dice_trim = self.message.split()
        try:
            dice_type = Dice.dice_dict[dice_trim[0]]
            if int(dice_trim[1]) <= 0:
                dice_1 = "You must specify a postive number of dice."
                return dice_1
            if int(dice_trim[1]) > 9:
                dice_2 = "You may only roll up to 9 dice."
                return dice_2
            for i in range(int(dice_trim[1])):
                str(random.randint(1, dice_type))            
            if int(dice_trim[1]) <= 9 and int(dice_trim[1]) > 0:
                return True

        except KeyError:
            return "That dice type does not exist!"
        except ValueError:
            return "Please enter only your dice type followed by a numerical value for number of rolls (Ex: /d20 4)"
        except IndexError:
            return "Please enter only your dice type followed by a numerical value for number of rolls (Ex: /d20 4)"

    def dice_roller(self, message):
        self.message = message
        dice_trim = self.message.split()
        
        Dice.dice_result.clear()
        dice_type = Dice.dice_dict[dice_trim[0]]
        for i in range(int(dice_trim[1])):
            Dice.dice_result.extend(str(random.randint(1, dice_type)))

# Coin flip
def flip():
    roll_result = random.randint(1, 2)
    if roll_result == 1:
        flip.heads += 1
        return "Heads"
    else:
        flip.tails += 1
        return "Tails"

flip.heads = 0
flip.tails = 0
