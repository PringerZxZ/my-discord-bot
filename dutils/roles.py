    if message.content.startswith('/addrole'):
        role_list = ["Bad_Bot", "Dumb_Bot", "Crazy_Bot"]
        entered_role = message.content[9:]
        role = discord.utils.get(message.guild.roles, name=entered_role)
        member = message.author
        roles = [
        # Role IDs
        #"581579864620924931",
        #"582957201258184715",
        #"582972597902245909",
        ]
        for r in message.author.roles:
            if r.id in roles:
                # If a role in the user's list of roles matches one of those we're checking
                await message.channel.send("You already have that role!")
                return
        if role is None or role.name not in role_list:
            await message.channel.send( "That role does not exist!")
            return
        elif role in message.author.roles:
            # If they already have the role
            await message.channel.send("You already have this role.")
        else:
            try:
                await member.add_roles(role)
                await message.channel.send("Successfully added role {0}".format(role.name))
            except discord.Forbidden:
                await message.channel.send("Invalid permissions")