# discord-bot

This is a basic bot with various functions used for learning Python. Capabilities and functions will change over time.

## Authentication

You must create a token.ini file in the directory where the bot is run. Your format can be as follows:

[Discord]

token: <tokenhere>